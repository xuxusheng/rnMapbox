interface Geometry {
    coordinates: any[]
    type: string
}

export interface Feature {
    geometry: Geometry
    properties: {
        screenPointX?: number
        screenPointY?: number
    }
    type: string
}
